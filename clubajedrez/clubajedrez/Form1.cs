﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace clubajedrez
{
    public partial class Form1 : Form
    {
        private Form formularioactivo = null;
        public Form1()
        {
            InitializeComponent();
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
           
        }

        private void button10_Click(object sender, EventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MenuVertical.Width = 63;
            MenuContenido.Width = 808;
            //MenuVertical.Visible = true;
        }

        private void btnMenu_Click(object sender, EventArgs e)
        {
            if (MenuVertical.Width == 200)
            {
                MenuVertical.Width = 63;
               
              
            }
            else
            {
                MenuVertical.Width = 200;
               
                
            }
        }

        private void btnMaximizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            btnRestaurar.Visible = true;
            btnMaximizar.Visible = false;
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            
        }

        private void btnCerrar_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnParticipantes_Click(object sender, EventArgs e)
        {
            prestamos( new Participantes());
        }
        private void prestamos(Form prestamos)
        {
            if (formularioactivo != null)
            {
                formularioactivo.Close();
            }
            formularioactivo = prestamos;
            prestamos.TopLevel = false;
            prestamos.FormBorderStyle = FormBorderStyle.None;
            prestamos.Dock = DockStyle.Fill;
            MenuContenido.Controls.Add(prestamos);
            MenuContenido.Tag = prestamos;
            prestamos.BringToFront();
            prestamos.Show();
        }
        private void MenuContenido_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnMaximizar_Click_1(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
    }
}
